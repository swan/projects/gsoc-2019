# CERN-HSF GSoC 2019
## Package manager for SWAN/Jupyterlab
## Exercise for Candidate Students

This repository contains an exercise to evaluate those students interested in applying for the *Package manager for SWAN/Jupyterlab* project, included in the Google Summer of Code (GSoC) program and offered by the [IT](http://information-technology.web.cern.ch/) department and the [EP-SFT](http://ep-dep-sft.web.cern.ch/) group at CERN. The detailed description of the project can be found [here](https://hepsoftwarefoundation.org/gsoc/2019/proposal_SWANPkgManager.html).

This exercise involves programming and it aims to test the student's skills with [JavaScript](https://www.javascript.com/) and [Jupyter notebooks](http://jupyter.org/)/[JupyterLab](https://jupyterlab.readthedocs.io).

Please follow the guidelines below to go through the exercise and work at a pace that suits you. Do not hesitate to ask us, the mentors, any question you might have.

### *TASK 1: Replace all the words "SWAN" with the SWAN logo*

The objective of this task is to write a [Jupyter Nbextension](http://jupyter-notebook.readthedocs.io/en/stable/extending/frontend_extensions.html) that replaces all the "SWAN" strings which are located inside *markdown cells* (inside a Notebook) with the [SWAN logo](logo_swan_cloudhisto.png). The image should only be displayed if the extension is active, therefore only the rendered output of a markdown cell should be replaced, not the content of the cell itself.

![SWAN text replacement](swan_image1.png)

In preparation of this task, you will need to:
* Install Jupyter on your machine and launch a local notebook server.
* Have basic understanding about notebooks, e.g. types of cells (markdown, code).
* Understand how to install and run an Nbextension.
* Have a look at examples of notebook extensions [here](https://github.com/ipython-contrib/jupyter_contrib_nbextensions/tree/master/src/jupyter_contrib_nbextensions/nbextensions).

#### *Deliverable*

The __deliverable__ of this task is a packaged Jupyter Nbextension ready to be installed, which provides the functionality described above.

### *TASK 2: Create a JupyterLab extension (optional)*

This second task is optional and aims to make the extension developed in Task 1 compatible with [JupyterLab](https://jupyterlab.readthedocs.io/en/stable/).

In preparation of this task, you will need to:
* Install JupyterLab on your machine and launch a local notebook server;
* Understand how to install and run a [JupyterLab extension](https://jupyterlab.readthedocs.io/en/stable/user/extensions.html);
* Understand the different types of extensions, their purpose and adequacy to the task;
* Have a look at examples of notebook extensions [here](https://github.com/mauhai/awesome-jupyterlab) or directly from the [JupyterLab repos](https://github.com/jupyterlab).

#### *Deliverable*

The __deliverable__ of this task is a packaged JupyterLab extension ready to be installed, which provides the functionality described above.


Once you complete any of the tasks of this exercise, please send us - by e-mail - the requested deliverable to:
diogo.castro@cern.ch, jakub.moscicki@cern.ch, enrico.bocchi@cern.ch and etejedor@cern.ch.
